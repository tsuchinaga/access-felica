### Access felica

下記環境でブラウザを通してFelicaの読み書きを行なう

## 環境

* OS : Windows 10 Pro
* ブラウザ : Google Chrome 75.0.3770.142
* デバイス : NFC Port/PaSoRi 100 USB (RC-S380/P)

上記以外環境構築 : [WindowsのブラウザでWebUSBを使えるのか - tsuchinaga](https://scrapbox.io/tsuchinaga/Windows%E3%81%AE%E3%83%96%E3%83%A9%E3%82%A6%E3%82%B6%E3%81%A7WebUSB%E3%82%92%E4%BD%BF%E3%81%88%E3%82%8B%E3%81%AE%E3%81%8B)
