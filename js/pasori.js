/**
 * @param {int[]} items
 */
function checksum(items) {
    let sum = 0;
    for (let i = 0; i < items.length; i++) {
        sum += items[i];
    }
    return 256 - sum % 256
}

/**
 * @param {int[]} params
 * @returns {int[]} packet
 */
function packet(params) {
    let packet = [];
    packet.push(0x00, 0x00, 0xff, 0xff, 0xff);
    packet.push(params.length + 1, 0x00, checksum([params.length + 1, 0x00]));
    packet.push(0xd6, ...params, checksum([0xd6, ...params]));
    packet.push(0x00);
    return packet
}

function init(device, outEndpoint) {
    return device.transferOut(outEndpoint, Uint16Array.of(0x00, 0x00, 0xff, 0x00, 0xff, 0x00))
}

function send(device, outEndpoint, data) {
    return device.transferOut(outEndpoint, Uint16Array.of(...packet(data)))
}

function read(device, inEndpoint, len) {
    return device.transferIn(inEndpoint, len)
}
